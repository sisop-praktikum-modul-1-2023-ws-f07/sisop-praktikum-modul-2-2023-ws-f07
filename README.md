Kelompok F07 :
| Nama | NRP |
| ---------------------- | ---------- |
| Muhammad Arkan Karindra Darvesh | 5025211236 |
| Dany Dary | 5025211237 |
| Meyroja Jovancha Firoos | 5025211204 |

## Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system.

#### Code
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main() {
    pid_t child_1, child_2, child_3, child_4, child_5, child_6;
    child_1 = fork();
    if(child_1 < 0) {
        printf("\nError: Failed to fork.\n");
    }
    else if(child_1 == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAir", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        child_2 = fork();
        if(child_2 == 0) {
            sleep(1);
            system("ls *.jpg | shuf -n 1");
            system("mv *air.jpg HewanAir");
            system("mv *darat.jpg HewanDarat");
            system("mv *amphibi.jpg HewanAmphibi");
        }
        else {
            child_3 = fork();
            if(child_3 == 0) {
                sleep(5);
                child_4 = fork();
                if(child_4 == 0) {
                    char *argv[] = {"zip","-r", "HewanAir.zip", "HewanAir", NULL};
                    execv("/bin/zip", argv);
                }
                else {
                    child_5 = fork();
                    if(child_5 == 0) {
                        char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
                        execv("/bin/zip", argv);
                    }
                    else {
                        child_6 = fork();
                        if(child_6 == 0) {
                            char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                            execv("/bin/zip", argv);
                        }
                        else {
                            sleep(1);
                            system("rm -rf HewanDarat");
                            system("rm -rf HewanAir");
                            system("rm -rf HewanAmphibi");
                            printf("All processes are completed and directories are removed.\n");
                        }
                    }
                }
            }
            else {
                system("wget -c 'https://drive.google.com/uc?exSport=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq' -O binatang.zip ");
                char *argv[] = {"unzip", "binatang.zip", NULL};
                execv("/bin/unzip", argv);
            }
        }
    }
    return 0;
}
```

#### Penjelasan Code
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
```
Kode ini merupakan library yang dibutuhkan.

#### Penjelasan Code
```c
int main() {
    pid_t child_1, child_2, child_3, child_4, child_5, child_6;
    child_1 = fork();
    if(child_1 < 0) {
        printf("\nError: Failed to fork.\n");
    }
```
Fungsi fork adalah untuk melakukan spawning proses.


#### Penjelasan Code
```c
else if(child_1 == 0) {
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAir", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
    }
```
Kode ini berfungsi untuk membuat 3 direktori yaitu HewanDarat, HewanAir, dan HewanAmphibi.

#### Penjelasan Code
```c
system("ls *.jpg | shuf -n 1");
system("mv *air.jpg HewanAir");
system("mv *darat.jpg HewanDarat");
system("mv *amphibi.jpg HewanAmphibi");
```
Kode ini berfungsi untuk memindahkan hewan sesuai dengan tempat hidupnya.

#### Penjelasan Code
```c
char *argv[] = {"zip","-r", "HewanAir.zip", "HewanAir", NULL};
```
Pada proses ini kita melukan zip pada folder yang telah kita buat sebelumnya. 


#### Penjelasan Code
```c
sleep(1);
system("rm -rf HewanDarat");
system("rm -rf HewanAir");
system("rm -rf HewanAmphibi");
printf("All processes are completed and directories are removed.\n");
```
Pada proses ini kita menghapus semua direktori yang sudah kita buat sebelumnya. 

#### Penjelasan Code
```c
system("wget -c 'https://drive.google.com/uc?exSport=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq' -O binatang.zip ");
```
Pada proses ini kita mengunduh file yang diberikan.


# soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

### Code
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>

void unzip(char *SourceDirectory)
{
  int stat;
  pid_t unzipID = fork();

  if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", SourceDirectory, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }
  while (wait(&stat) > 0)
    ;

  unzipID = fork();
  if (unzipID == 0)
  {
    char *args[] = {"rm", "-f", SourceDirectory, NULL};
    execv("/bin/rm", args);
  }
  while (wait(&stat) > 0)
    ;

  waitpid(unzipID, &stat, 0);
}

void download(char *url, char *filename)
{
  int stat;
  pid_t IdDownload = fork();

  if (IdDownload == 0)
  {
    char *args[] = {"wget", "--no-check-certificate", url, "-O", filename, NULL};
    execv("/usr/bin/wget", args);
  }
  waitpid(IdDownload, &stat, 0);
}

void moveByCategory()
{
  int stat;
  char *path;
  pid_t child_id[4];
  struct dirent *dp;
  DIR *folder;

  for (int i = 0; i < 4; i++)
  {
    char *newFolder;
    switch (i)
    {
    case 0:
      newFolder = "Bek";
      break;
    case 1:
      newFolder = "Gelandang";
      break;
    case 2:
      newFolder = "Kiper";
      break;
    case 3:
      newFolder = "Penyerang";
      break;
    }
    child_id[i] = fork();
    if (child_id[i] < 0)
    {
      printf("gagal dalam membuat child process.\n");
      exit(EXIT_FAILURE);
    }
    else if (child_id[i] == 0)
    {
      char *args[] = {"mkdir", "-p", newFolder, NULL};
      execv("/bin/mkdir", args);
    }

    while (wait(&stat) > 0)
      ;

    if (folder != NULL)
    {
      folder = opendir("players");
      while ((dp = readdir(folder)) != NULL)
      {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
        {
          if (dp->d_type == DT_REG && strstr(dp->d_name, newFolder) != NULL)
          {
            path = malloc(strlen("players") + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", "players", dp->d_name);
            child_id[i] = fork();
            if (child_id[i] == 0)
            {
              char *args[] = {"mv", "-f", path, newFolder, NULL};
              execv("/bin/mv", args);
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < 4; i++)
  {
    waitpid(child_id[i], &stat, 0);
  }
  closedir(folder);
}

void removeExceptMU(char *directory)
{
  int stat;
  char *path;
  id_t child_id;
  struct dirent *dp;
  DIR *folder;
  folder = opendir(directory);

  if (folder != NULL)
  {
    int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
      {
        if (dp->d_type == DT_REG)
        {
          if (strstr(dp->d_name, "ManUtd") == NULL)
          {
            path = malloc(strlen(directory) + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", directory, dp->d_name);
            child_id = fork();
            if (child_id == 0)
            {
              char *args[] = {"rm", "-f", path, NULL};
              execv("/bin/rm", args);
            }
          }
        }
      }
    }
  }
  closedir(folder);
  waitpid(child_id, &stat, 0);
}

void deleteFolder()
{
  int stat;
  pid_t child_id = fork();
  if (child_id == 0)
  {
    char *args[] = {"rm", "-rf", "players", NULL};
    execv("/bin/rm", args);
  }
  waitpid(child_id, &stat, 0);
}

int main()
{
  /*Program filter.c pertama-tama akan mengunduh file yang berisi database pemain sepak bola, kemudian mengekstrak "players.zip."
    Hapus file zip tersebut setelah itu agar tidak menghabiskan ruang pada PC Ten Hag.*/

  download("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
  unzip("players.zip");

  /* Karena database yang diunduh berisi data mentah. Maka selanjutnya kita akan menghapus
     semua pemain dari direktori yang bukan berasal dari Manchester United.*/
  removeExceptMU("players");

 /* Setelah mengetahui nama-nama pemain Manchester United, 
    maka kita perlu mengkategorikan mereka sesuai dengan posisi mereka.
    Akan ada empat kategori folder: kiper, bek, gelandang, dan penyerang.*/
  moveByCategory();
  deleteFolder();

  /*Mengikuti kategorisasi anggota tim Manchester United, 
    Ten Hag mengharuskan Tim Terbaik untuk menjadi senjata utama MU berdasarkan peringkat terbaik 
    dengan wajib kiper, pemain bertahan, pemain tengah, dan penyerang. Dengan outpu seperti dibawah ini
    Formasi [jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt akan dibuat dan disimpan di /home/[users]/ */

  return 0;
}
```
###Penjelasan Code
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>
```
Baris kode ini merupakan bagian dari header file yang digunakan untuk mengimpor library standar yang dibutuhkan dalam program ini.

```c
int stat;
  pid_t unzipID = fork();

  if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", SourceDirectory, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }
  ```
  Fungsi pertama adalah fungsi unzip. Fungsi ini memiliki tujuan untuk mengunzip folder yang diberikan. 
Pertama - taman fungsi membuat childprocess menggunakan fork lalu mengeksekusi command unzip menggunakan execv sesuai dengan parameternya.
Lalu fungsi akan menungggu hingga childprocess kelar menggunakan wait :
```c
while (wait(&stat) > 0)
```
Setelah childprocess diatas selesai lalu fungsi akan lanjut menghapus file zip tadi menggunakan command rm :
```c
if (unzipID == 0)
  {
    char *args[] = {"rm", "-f", SourceDirectory, NULL};
    execv("/bin/rm", args);
  }
  ```
  Sama seperti child process sebelumnya, fungsi akan menunggu sampai childprocess ini selesai menggunakan command wait : 
  ```c
  while (wait(&stat) > 0)
  ```
  Selanjutnya, ada fungsi download, fungsinya buat ngedownload directorynya.jadi selanjutnya fungsi pertama itu kita buat child process gunain fork trus di child proccessnya kita kasi perintah excecv untuk ngeksekusi perintah wget buat ngedownload directorynya :
  ```c
{
    char *args[] = {"wget", "--no-check-certificate", url, "-O", filename, NULL};
    execv("/usr/bin/wget", args);
    ```
abis itu ada waitpid ini buat nunggu child proccessnya selesai :
```c
 waitpid(IdDownload, &stat, 0);
 ```
 Selanjutnya ada fungsi moveByCategory yang memiliki tujuan untuk memindahkan file gambar ke dalam directory yang sesuai. 
Hal pertama yang dilakukan adalah memberi perintah untuk membuat directory bek, gelandang, kiper, dan penyerang menggunakan command mkdir :
```c
{
      char *args[] = {"mkdir", "-p", newFolder, NULL};
      execv("/bin/mkdir", args);
    }
```
Setelah itu fungsi akan membaca file gambar menggunakan readdir dan opendir, untuk setiap gambar :
```c
folder = opendir("players");
      while ((dp = readdir(folder)) != NULL)
```
Fungsi ini menentukan kategorinya berdasarkan nama dan memindahkannya menggunakan command mv :
```c
char *args[] = {"mv", "-f", path, newFolder, NULL};
              execv("/bin/mv", args);
```
Terakhir fungsi menunggu semua childprocess untuk selesai menggunakan waitpid :
```c
for (int i = 0; i < 4; i++)
  {
    waitpid(child_id[i], &stat, 0);
  }
```
Lalu ada fungsi removeExceptMU yang memiliki tujuan untuk menghilangkan semua pemain kecuali yang terdapat nama “ManUtd”. 
Pertama - tama fungsi akan mengiterasi semua file dan mengecek apakah file tersebut berformat png atau tidak :
```c
if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
```
 jika iya fungsi lanjut mengecek apakah di file tersebut terdapat string “ManUtd”  :
 ```c
 if (strstr(dp->d_name, "ManUtd") == NULL)
 ```
 jika tidak maka akan dilakukan penghapusan menggunakan command rm :
 ```c
if (child_id == 0)
            {
              char *args[] = {"rm", "-f", path, NULL};
              execv("/bin/rm", args);
            }
```
Fungsi deleteFolder memiliki tujuan untuk menghapus directory players menggunakan command rm :
```
char *args[] = {"rm", "-rf", "players", NULL};
    execv("/bin/rm", args);
```



## Soal 4

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa **Jam** (0-23), **Menit** (0-59), **Detik** (0-59), **Tanda asterisk [ * ]** (value bebas), serta path **file .sh**.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan **pesan “error”** apabila argumen yang diterima program tidak sesuai. **Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.**
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini **berjalan dalam background** dan **hanya menerima satu config cron.**
- Bonus poin apabila CPU state minimum.

**Contoh untuk run**: /program \* 44 5 /home/Banabil/programcron.sh

#### Deskripsi

Kode tersebut merupakan sebuah program dalam bahasa pemrograman C yang dapat digunakan untuk menjalankan sebuah perintah shell secara otomatis pada waktu tertentu yang ditentukan oleh pengguna. 

#### Code

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    char cmd[1024];
    char arg[1024];
    int hour, minute, second;

    // Check number of arguments
    if (argc != 6) {
        fprintf(stderr, "Error number of arguments\n");
        return 1;
    }

    // Parse input arguments
    hour = atoi(argv[1]);
    minute = atoi(argv[2]);
    second = atoi(argv[3]);

    // Validate input arguments
    if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59) {
        fprintf(stderr, "Error time format\n");
        return 1;
    }

    if (strcmp(argv[4], "*") != 0) {
        fprintf(stderr, "Error Invalid\n");
        return 1;
    }

    // Prepare command to be executed
    sprintf(cmd, "%02d:%02d:%02d %s", hour, minute, second, argv[5]);

    // Execute command in background
    if (fork() == 0) {
        execlp("sh", "sh", "-c", cmd, NULL);
        exit(0);
    }

    return 0;
}
```
### Penjelasan Code
```c 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h> 
```
Baris kode ini merupakan bagian dari header file yang digunakan untuk mengimpor library standar yang dibutuhkan dalam program ini.

```c 
int main(int argc, char *argv[]) {
    char cmd[1024];
    char arg[1024];
    int hour, minute, second;
```
Fungsi main() adalah fungsi utama dalam program C. Pada program ini, fungsi ini memiliki tiga variabel integer yaitu hour, minute, dan second. Selain itu, ada juga dua variabel array of char, yaitu cmd dan arg.

```
if (argc != 6) {
        fprintf(stderr, "Error number of arguments\n");
        return 1;
}
```
Bagian ini digunakan untuk memeriksa apakah jumlah argumen yang diinputkan benar. Jika jumlah argumen kurang atau lebih dari enam, maka program akan memberikan pesan error dan berhenti dengan return 1.

```
hour = atoi(argv[1]);
minute = atoi(argv[2]);
second = atoi(argv[3]);
```
Bagian ini akan mengambil input waktu dalam format hh:mm:ss yang diberikan oleh pengguna. Kemudian input tersebut akan diubah menjadi tipe data integer menggunakan fungsi atoi().

```
if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59) {
        fprintf(stderr, "Error time format\n");
        return 1;
}
```
Setelah input waktu diubah menjadi tipe data integer, program akan memeriksa apakah input waktu tersebut valid atau tidak. Jika waktu yang dimasukkan tidak valid, program akan memberikan pesan error dan berhenti dengan return 1.

```
if (strcmp(argv[4], "*") != 0) {
        fprintf(stderr, "Error Invalid\n");
        return 1;
}
```
Program akan memeriksa apakah argumen keempat adalah tanda bintang. Jika bukan, program akan memberikan pesan error dan berhenti dengan return 1.

```
sprintf(cmd, "%02d:%02d:%02d %s", hour, minute, second, argv[5]);
```
Program akan membuat sebuah string command untuk menjalankan perintah shell. Format dari command tersebut adalah hh:mm:ss perintah_shell.

```
if (fork() == 0) {
        execlp("sh", "sh", "-c", cmd, NULL);
        exit(0);
}
```
Program akan membuat proses baru dengan menggunakan fungsi fork(). Proses baru tersebut akan menjalankan perintah shell yang telah dibuat sebelumnya dengan menggunakan fungsi execlp(). Setelah proses selesai, proses tersebut akan keluar dari program dengan exit(0).

Dengan demikian, program akan berakhir dengan nilai 0 jika berhasil menjalankan perintah shell, dan akan berakhir dengan nilai 1 jika terjadi kesalahan pada input waktu atau argumen.

### Output
Untuk menjalankan program tersebut, Anda perlu meng-compile terlebih dahulu dengan perintah gcc pada terminal dengan cara seperti berikut:
```
gcc program.c -o program
```
Setelah berhasil di-compile, Anda dapat menjalankan program dengan cara seperti berikut:

Menjalankan program dengan input waktu yang valid dan perintah yang benar:
```
./program 12 30 00 * "ls -l"
```
Program akan menjalankan perintah ls -l pada pukul 12:30:00.

Menjalankan program dengan input waktu yang tidak valid:
```
./program 24 60 60 * "ls -l"
```
Program akan mengeluarkan pesan kesalahan "Error time format".

Menjalankan program dengan input argumen yang tidak valid:
```
./program 12 30 00 day "ls -l"
```
Program akan mengeluarkan pesan kesalahan "Error Invalid".

Menjalankan program dengan kurang/more argumen:
```
./program 12 30 00 *
```
Program akan mengeluarkan pesan kesalahan "Error number of arguments".
