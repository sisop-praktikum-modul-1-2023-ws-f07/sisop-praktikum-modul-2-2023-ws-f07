#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main() {
    pid_t child_1, child_2, child_3, child_4, child_5, child_6;
    child_1 = fork();
    if(child_1 < 0) {
        printf("\nError: Failed to fork.\n");
    }
    else if(child_1 == 0) {
        // Create three directories
        char *argv[] = {"mkdir", "-p", "HewanDarat", "HewanAir", "HewanAmphibi", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        child_2 = fork();
        if(child_2 == 0) {
            // Wait for the directories to be created
            sleep(1);
            // Sort and move downloaded images
            system("ls *.jpg | shuf -n 1");
            system("mv *air.jpg HewanAir");
            system("mv *darat.jpg HewanDarat");
            system("mv *amphibi.jpg HewanAmphibi");
        }
        else {
            child_3 = fork();
            if(child_3 == 0) {
                // Wait for the images to be sorted and moved
                sleep(5);
                // Compress the directories of the sorted images
                child_4 = fork();
                if(child_4 == 0) {
                    char *argv[] = {"zip","-r", "HewanAir.zip", "HewanAir", NULL};
                    execv("/bin/zip", argv);
                }
                else {
                    child_5 = fork();
                    if(child_5 == 0) {
                        char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
                        execv("/bin/zip", argv);
                    }
                    else {
                        child_6 = fork();
                        if(child_6 == 0) {
                            char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                            execv("/bin/zip", argv);
                        }
                        else {
                            // Wait for the directories to be compressed
                            sleep(1);
                            // Remove the directories
                            system("rm -rf HewanDarat");
                            system("rm -rf HewanAir");
                            system("rm -rf HewanAmphibi");
                            printf("All processes are completed and directories are removed.\n");
                        }
                    }
                }
            }
            else {
                // Download and extract images
                system("wget -c 'https://drive.google.com/uc?exSport=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq' -O binatang.zip ");
                char *argv[] = {"unzip", "binatang.zip", NULL};
                execv("/bin/unzip", argv);
            }
        }
    }
    return 0;
}
