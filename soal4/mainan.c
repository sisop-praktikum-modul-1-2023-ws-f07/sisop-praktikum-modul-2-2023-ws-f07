#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    char cmd[1024];
    char arg[1024];
    int hour, minute, second;

    // Check number of arguments
    if (argc != 6) {
        fprintf(stderr, "Error number of arguments\n");
        return 1;
    }

    // Parse input arguments
    hour = atoi(argv[1]);
    minute = atoi(argv[2]);
    second = atoi(argv[3]);

    // Validate input arguments
    if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59) {
        fprintf(stderr, "Error time format\n");
        return 1;
    }

    if (strcmp(argv[4], "*") != 0) {
        fprintf(stderr, "Error Invalid\n");
        return 1;
    }

    // Prepare command to be executed
    sprintf(cmd, "%02d:%02d:%02d %s", hour, minute, second, argv[5]);

    // Execute command in background
    if (fork() == 0) {
        execlp("sh", "sh", "-c", cmd, NULL);
        exit(0);
    }

    return 0;
}
