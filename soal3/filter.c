#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>

void unzip(char *SourceDirectory)
{
  int stat;
  pid_t unzipID = fork();

  if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", SourceDirectory, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }
  while (wait(&stat) > 0)
    ;

  unzipID = fork();
  if (unzipID == 0)
  {
    char *args[] = {"rm", "-f", SourceDirectory, NULL};
    execv("/bin/rm", args);
  }
  while (wait(&stat) > 0)
    ;

  waitpid(unzipID, &stat, 0);
}

void download(char *url, char *filename)
{
  int stat;
  pid_t IdDownload = fork();

  if (IdDownload == 0)
  {
    char *args[] = {"wget", "--no-check-certificate", url, "-O", filename, NULL};
    execv("/usr/bin/wget", args);
  }
  waitpid(IdDownload, &stat, 0);
}

void moveByCategory()
{
  int stat;
  char *path;
  pid_t child_id[4];
  struct dirent *dp;
  DIR *folder;

  for (int i = 0; i < 4; i++)
  {
    char *newFolder;
    switch (i)
    {
    case 0:
      newFolder = "Bek";
      break;
    case 1:
      newFolder = "Gelandang";
      break;
    case 2:
      newFolder = "Kiper";
      break;
    case 3:
      newFolder = "Penyerang";
      break;
    }
    child_id[i] = fork();
    if (child_id[i] < 0)
    {
      printf("gagal dalam membuat child process.\n");
      exit(EXIT_FAILURE);
    }
    else if (child_id[i] == 0)
    {
      char *args[] = {"mkdir", "-p", newFolder, NULL};
      execv("/bin/mkdir", args);
    }

    while (wait(&stat) > 0)
      ;

    if (folder != NULL)
    {
      folder = opendir("players");
      while ((dp = readdir(folder)) != NULL)
      {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
        {
          if (dp->d_type == DT_REG && strstr(dp->d_name, newFolder) != NULL)
          {
            path = malloc(strlen("players") + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", "players", dp->d_name);
            child_id[i] = fork();
            if (child_id[i] == 0)
            {
              char *args[] = {"mv", "-f", path, newFolder, NULL};
              execv("/bin/mv", args);
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < 4; i++)
  {
    waitpid(child_id[i], &stat, 0);
  }
  closedir(folder);
}

void removeExceptMU(char *directory)
{
  int stat;
  char *path;
  id_t child_id;
  struct dirent *dp;
  DIR *folder;
  folder = opendir(directory);

  if (folder != NULL)
  {
    int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
      {
        if (dp->d_type == DT_REG)
        {
          if (strstr(dp->d_name, "ManUtd") == NULL)
          {
            path = malloc(strlen(directory) + strlen(dp->d_name) + 2);
            sprintf(path, "%s/%s", directory, dp->d_name);
            child_id = fork();
            if (child_id == 0)
            {
              char *args[] = {"rm", "-f", path, NULL};
              execv("/bin/rm", args);
            }
          }
        }
      }
    }
  }
  closedir(folder);
  waitpid(child_id, &stat, 0);
}

void deleteFolder()
{
  int stat;
  pid_t child_id = fork();
  if (child_id == 0)
  {
    char *args[] = {"rm", "-rf", "players", NULL};
    execv("/bin/rm", args);
  }
  waitpid(child_id, &stat, 0);
}

int main()
{
  /*Program filter.c pertama-tama akan mengunduh file yang berisi database pemain sepak bola, kemudian mengekstrak "players.zip."
    Hapus file zip tersebut setelah itu agar tidak menghabiskan ruang pada PC Ten Hag.*/

  download("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
  unzip("players.zip");

  /* Karena database yang diunduh berisi data mentah. Maka selanjutnya kita akan menghapus
     semua pemain dari direktori yang bukan berasal dari Manchester United.*/
  removeExceptMU("players");

 /* Setelah mengetahui nama-nama pemain Manchester United, 
    maka kita perlu mengkategorikan mereka sesuai dengan posisi mereka.
    Akan ada empat kategori folder: kiper, bek, gelandang, dan penyerang.*/
  moveByCategory();
  deleteFolder();

  /*Mengikuti kategorisasi anggota tim Manchester United, 
    Ten Hag mengharuskan Tim Terbaik untuk menjadi senjata utama MU berdasarkan peringkat terbaik 
    dengan wajib kiper, pemain bertahan, pemain tengah, dan penyerang. Dengan outpu seperti dibawah ini
    Formasi [jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt akan dibuat dan disimpan di /home/[users]/ */

  return 0;
}

